package com.souvik.TaskScheduler.service;

import com.souvik.TaskScheduler.domain.Task;
import com.souvik.TaskScheduler.domain.TaskExecutionStatus;
import com.souvik.TaskScheduler.repository.TaskExecutionStatusInsertRepository;
import com.souvik.TaskScheduler.util.CommonUtil;
import com.souvik.TaskScheduler.util.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.sql.Timestamp;
import java.util.HashMap;
import java.util.Map;

@Component
public class TaskModifier {
    private Map<String, Task> taskMap;
    private Map<Long, Map<String, Task>> taskMapAtTime;
    @Autowired
    TaskExecutionStatusInsertRepository taskExecutionStatusInsertRepository;

    public TaskModifier() {
        taskMap = new HashMap<>();
        taskMapAtTime = new HashMap<>();
    }

    public Map<Long, Map<String, Task>> getTaskMapAtTime() {
        return taskMapAtTime;
    }
    public Map<String, Task> getTaskMap() {
        return taskMap;
    }

    public void addTask(Task task) {
        if(!taskMap.containsKey(task.getId())) {
            taskMap.put(task.getId(), task);
        }
        Long nextRun = CommonUtil.getNextRunForCronExp(task.getCronExp());
        if(taskMapAtTime.containsKey(nextRun)) {
            taskMapAtTime.get(nextRun).put(task.getId(), task);
        }
        else {
            Map<String, Task> taskAtTime = new HashMap<>();
            taskAtTime.put(task.getId(), task);
            taskMapAtTime.put(nextRun, taskAtTime);
        }
    }

    public void changeStatus(String taskId) {
        if(!taskMap.containsKey(taskId)) {
            System.out.println("Task id  - " + taskId + " not available in db");
        }
        synchronized(taskMap.get(taskId)) {
            Task task = taskMap.get(taskId);

            String curStatus = task.getState();
            if(curStatus.equals(Constants.ACTIVE)) {
                Long nextRun = CommonUtil.getNextRunForCronExp(task.getCronExp());
                synchronized(taskMapAtTime) {
                    if(taskMapAtTime.containsKey(nextRun)) {
                        Map<String, Task> tasksAtNextRun = taskMapAtTime.get(nextRun);
                        for (Map.Entry<String, Task> entry : tasksAtNextRun.entrySet()) {
                            String tId = entry.getKey();
                            if (tId.equals(taskId)) {
                                tasksAtNextRun.remove(tId);
                                break;
                            }
                        }
                    }
                }

                task.setState(Constants.INACTIVE);
                System.out.println("Deactivating task - " + taskId);
                taskExecutionStatusInsertRepository.insertWithEntityManager(new TaskExecutionStatus(task.getId(),
                        Constants.INACTIVE, new Timestamp(System.currentTimeMillis()),null, Constants.SUCCESSFUL, task.getPriority(), task.getType()));
            }
            else if (curStatus.equals(Constants.INACTIVE)){
                task.setState(Constants.ACTIVE);
                Long nextRun = CommonUtil.getNextRunForCronExp(task.getCronExp());
                synchronized(taskMapAtTime) {
                    if(taskMapAtTime.containsKey(nextRun)) {
                        Map<String, Task> tasksAtNextRun = taskMapAtTime.get(nextRun);
                        tasksAtNextRun.put(taskId, task);
                    }
                    else {
                        Map<String, Task> taskAtTime = new HashMap<>();
                        taskAtTime.put(taskId, task);
                        taskMapAtTime.put(nextRun, taskAtTime);
                    }
                }
            }
        }

    }
}
