package com.souvik.TaskScheduler.service;

import com.souvik.TaskScheduler.domain.Task;
import com.souvik.TaskScheduler.domain.TaskExecutionStatus;
import com.souvik.TaskScheduler.repository.TaskExecutionStatusInsertRepository;
import com.souvik.TaskScheduler.util.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;

@Service
public class TaskRunner {
    @Autowired
    TaskModifier taskModifier;
    @Autowired
    QueueService queueService;
    @Autowired
    TaskExecutionStatusInsertRepository taskExecutionStatusInsertRepository;

    @Async
    public void run() throws InterruptedException {
        while(true) {
            Task task = queueService.get();
            Timestamp current_time= new Timestamp(System.currentTimeMillis());
            System.out.println("Running task - " + task.getId());
            Thread.sleep(task.getDuration()*1000);
            System.out.println("Task" + task.getId() + " completed");
            synchronized(task) {
                if(task.getType().equals(Constants.TYPEB) && task.getState().equals(Constants.ACTIVE)) {
                    taskModifier.addTask(task);
                    taskExecutionStatusInsertRepository.insertWithEntityManager(new TaskExecutionStatus(task.getId(),
                            Constants.ACTIVE, current_time, new Timestamp(System.currentTimeMillis()), Constants.SUCCESSFUL, task.getPriority(), task.getType()));
                }
                else if(task.getType().equals(Constants.TYPEA)){
                    task.setState(Constants.COMPLETED);
                    taskExecutionStatusInsertRepository.insertWithEntityManager(new TaskExecutionStatus(task.getId(),
                            Constants.COMPLETED, current_time, new Timestamp(System.currentTimeMillis()), Constants.SUCCESSFUL, task.getPriority(), task.getType()));
                }
            }
        }
    }
}
