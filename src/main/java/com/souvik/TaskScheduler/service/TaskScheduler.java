package com.souvik.TaskScheduler.service;

import com.souvik.TaskScheduler.domain.Task;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class TaskScheduler {
    @Autowired
    TaskModifier taskModifier;
    @Autowired
    QueueService queueService;

    @Async
    public void run() throws InterruptedException {
        while(true) {
            long currTime = Instant.now().getEpochSecond();
            synchronized(taskModifier.getTaskMapAtTime()) {
                if(taskModifier.getTaskMapAtTime().containsKey(currTime)) {
                    List<Task> tasks = taskModifier.getTaskMapAtTime().get(currTime).values()
                            .stream()
                            .sorted(Comparator.comparing(e -> e.getPriority()))
                            .collect(Collectors.toList());
                    taskModifier.getTaskMapAtTime().remove(currTime);

                    for (Task t : tasks) {
                        queueService.add(t);
                    }
                }
            }
        }
    }
}
