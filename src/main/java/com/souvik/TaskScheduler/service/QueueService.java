package com.souvik.TaskScheduler.service;

import com.souvik.TaskScheduler.domain.Task;
import org.springframework.stereotype.Service;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.TimeUnit;

@Service
public class QueueService {
    private BlockingQueue<Task> queue;
    public QueueService() {
        this.queue = new LinkedBlockingDeque();
    }

    public void add(Task task) throws InterruptedException {
        queue.put(task);
    }

    public Task get() throws InterruptedException {
        return queue.poll(365, TimeUnit.DAYS);
    }
}
