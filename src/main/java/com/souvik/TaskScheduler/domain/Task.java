package com.souvik.TaskScheduler.domain;


import com.souvik.TaskScheduler.util.CommonUtil;

public class Task {
    private String id;
    private String type;
    private int priority;
    private int duration;
    private String state;
    private String cronExp;

    public Task(String type, int priority, int duration, String cronExp) {
        this.id = CommonUtil.getTaskId();
        this.type = type;
        this.priority = priority;
        this.duration = duration;
        this.cronExp = cronExp;
        this.state = "ACTIVE";
    }

    public String getId() {
        return id;
    }

    public void setId() {
        this.id = CommonUtil.getTaskId();
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCronExp() {
        return cronExp;
    }

    public void setCronExp(String cronExp) {
        this.cronExp = cronExp;
    }
}
