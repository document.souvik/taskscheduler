package com.souvik.TaskScheduler.domain;


import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.sql.Timestamp;

@Entity
public class TaskExecutionStatus {
    @Id
    @GeneratedValue
    private int id;
    private String task_id;
    private String state;
    private int priority;
    private String type;
    private Timestamp start_time;
    private Timestamp end_time;
    private String status;

    public TaskExecutionStatus(String task_id, String state, Timestamp start_time, Timestamp end_time, String status,
                               int priority, String type) {
        this.task_id = task_id;
        this.state = state;
        this.start_time = start_time;
        this.end_time = end_time;
        this.status = status;
        this.priority = priority;
        this.type = type;
    }
}
