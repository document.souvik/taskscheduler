package com.souvik.TaskScheduler.util;

public class Constants {
    public static final String INACTIVE = "INACTIVE";
    public static final String TYPEA = "A";
    public static final String TYPEB = "B";
    public static final String COMPLETED = "COMPLETED";
    public static final String SUCCESSFUL = "SUCCESSFUL";
    public static String ACTIVE = "ACTIVE";
}
