package com.souvik.TaskScheduler.util;

import com.sun.jmx.snmp.Timestamp;
import org.springframework.scheduling.support.CronSequenceGenerator;
import org.springframework.scheduling.support.CronTrigger;
import org.springframework.scheduling.support.SimpleTriggerContext;

import java.util.Date;

public class CommonUtil {
    private static int taskId = 0;

    public static int getPriority(String priority) {
        if(priority.equalsIgnoreCase("HIGH")) {
            return 3;
        }
        else if(priority.equalsIgnoreCase("MEDIUM")) {
            return 2;
        }
        else {
            return 1;
        }
    }

    public static String getTaskId() {
        return "t" + ++taskId;
    }

    public static Long getNextRunForCronExp(String cronExp) {
        CronSequenceGenerator cronTrigger = new CronSequenceGenerator(cronExp);
        Date next = cronTrigger.next(new Date());
        return next.getTime()/1000;
    }
}
