package com.souvik.TaskScheduler.repository;

import com.souvik.TaskScheduler.domain.TaskExecutionStatus;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

@Repository
public class TaskExecutionStatusInsertRepository {
    @PersistenceContext
    private EntityManager entityManager;

    @Transactional
    public void insertWithEntityManager(TaskExecutionStatus taskExecutionStatus) {
        this.entityManager.persist(taskExecutionStatus);
    }
}
