package com.souvik.TaskScheduler.resource;

import com.souvik.TaskScheduler.domain.Task;
import com.souvik.TaskScheduler.service.TaskModifier;
import com.souvik.TaskScheduler.service.TaskRunner;
import com.souvik.TaskScheduler.service.TaskScheduler;
import com.souvik.TaskScheduler.util.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;


@RestController
public class Scheduler {

    @Autowired
    TaskModifier taskModifier;
    @Autowired
    TaskRunner taskRunner;

    @Autowired
    TaskScheduler taskScheduler;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public String addTask(@RequestBody Task task) throws InterruptedException {
        taskModifier.addTask(task);
        taskScheduler.run();
        taskRunner.run();
        return "Task " + task.getId() + " added";
    }

    @PutMapping("changeStatus/{id}")
    public String changeStatus(@PathVariable String id) {
        taskModifier.changeStatus(id);
        return "Status changed";
    }

    @GetMapping
    public List<String> getActiveTasks() {
        return taskModifier.getTaskMap().values()
                .stream()
                .filter(t -> t.getState().equals(Constants.ACTIVE))
                .map(t -> t.getId())
                .collect(Collectors.toList());
    }

}
